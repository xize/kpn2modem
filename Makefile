include $(TOPDIR)/rules.mk

PKG_NAME:=KPN2mod
PKG_VERSION:=0.0.1
PKG_RELEASE:=1

PKG_BUILD_DIR:=$(BUILD_DIR)/KPN2mod-$(PKG_VERSION)
PKG_SOURCE:=KPN2mod-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://gitlab.com/xize/kpn2modem/
PKG_HASH:=<snip to be added later>

include $(INCLUDE_DIR)/package.mk

define Package/KPN2mod
  SECTION:=base
  CATEGORY:=Network
  TITLE:=KPN2Modem
  #DESCRIPTION:=This variable is obsolete. use the Package/name/description define instead!
  URL:=https://gitlab.com/xize/kpn2modem/
endef

define Package/bridge/description
  turns a default configurated openwrt into a working modem for the KPN internet service provider.

	adding support for voip, iptv, and internet.
endef

define Build/Configure
  $(call Build/Configure/Default,--with-linux-headers=$(LINUX_DIR))
endef

define Package/KPN2mod/install
      $(INSTALL_DIR) $(1)/usr/sbin
			$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/KPN2mod/kpn2mod $(1)/usr/sbin/kpn2mod
endef

$(eval $(call BuildPackage,KPN2mod))
